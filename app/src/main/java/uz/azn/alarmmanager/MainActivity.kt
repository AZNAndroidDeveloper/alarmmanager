package uz.azn.alarmmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

import uz.azn.alarmmanager.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        with(binding) {
            btnSetTime.setOnClickListener {
                val popTime = PopTime()
                val fragmentManager = supportFragmentManager
                popTime.show(fragmentManager, "Select Time")
            }
        }
    }
    fun setTime(hour: Int, minute: Int) {
        Log.d("TAG", "setTime: $hour $minute")
        tv_time.text = " $hour : $minute"
val saveDate = SaveData(applicationContext)
        saveDate.setAlarm(hour, minute)
    }

}