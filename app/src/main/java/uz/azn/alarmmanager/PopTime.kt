package uz.azn.alarmmanager

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import uz.azn.alarmmanager.databinding.PopTimeBinding

class PopTime:DialogFragment(){
    private lateinit var binding: PopTimeBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myView = inflater.inflate(R.layout.pop_time,container,false)
        val btnDan = myView.findViewById<TextView>(R.id.btn_dan)
        val tp1 = myView.findViewById<TimePicker>(R.id.tp1)
        btnDan.setOnClickListener {
            val activty = activity as MainActivity
            if (Build.VERSION.SDK_INT>=23){
                activty.setTime(tp1.hour, tp1.minute)
            }
            else
                activty.setTime(tp1.currentHour,tp1.currentMinute)
            this.dismiss()
        }
        return myView
    }


}