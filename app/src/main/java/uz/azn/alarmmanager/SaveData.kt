package uz.azn.alarmmanager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

class SaveData {

    var context:Context? = null
    constructor(context: Context){
        this.context = context
    }
    fun setAlarm (hour:Int, minute:Int){
        val calendar  = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY,hour)
        calendar.set(Calendar.MINUTE,minute)
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
val intent  =Intent(context,MyBroadcastReceiver::class.java)
        intent.putExtra("message","Alarm is working")
        intent.action = "uz.azn.alarmmanager"
        val pi  = PendingIntent.getBroadcast(context,0,intent, PendingIntent.FLAG_UPDATE_CURRENT)
        // bizlarni vaqt kelganda ishga tushuardi yani broadcastni
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.timeInMillis,AlarmManager.INTERVAL_DAY,pi)
    }
}